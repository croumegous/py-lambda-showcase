import petname


def handler(ctx, params):
    name = params.get('name', 'World')
    ctx.logger.log(f"[INFO] function started for {name}")
    return {
        "message": f"Hello {name}!",
        "random_name": petname.Generate(2, "-", 6)
    }

if __name__ == '__main__':
    import sys
    class Logger:
        def log(self, msg):
            print(msg, file=sys.stderr)
    class Context(dict):
        def __init__(self, *args, **kwargs):
            super(Context, self).__init__(*args, **kwargs)
            
        def __getattribute__(self, __name: str):
            return self[__name]
    handler(Context({"logger": Logger()}),{})